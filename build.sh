#!/usr/bin/env bash

# Build multiplatform images from specified base image.
# Requires being logged to Docker registry.
# Supply base image name as first, mandatory argument.
# Specify optionally a tag regex filter as second argument.

set -euo pipefail

# Get current script directory.
CURRENT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
export CURRENT_DIR

# Use Buildkit enhanced features.
export DOCKER_BUILDKIT=1

: "${DOCKER_REGISTRY:=${CI_REGISTRY:-docker.io}}"
: "${DOCKER_REGISTRY_USER:=${CI_REGISTRY_USER:-magenolia}}"
: "${DOCKER_REGISTRY_REPO:=${CI_PROJECT_NAME:-$(basename "$(pwd)")}}"
: "${DOCKER_BUILD_PLATFORMS:=${CI_BUILD_PLATFORMS:-linux/amd64,linux/arm64/v8}}"

build_push() {
  local php_version composer_version with_xdebug tag_name

  php_version="${1:-7.4}"
  composer_version="${2:-2}"
  with_xdebug="${3:-0}"
  tag_name="${DOCKER_REGISTRY}/${DOCKER_REGISTRY_USER}/${DOCKER_REGISTRY_REPO}:${php_version}-composer-${composer_version}"

  if [ "${with_xdebug}" = '1' ]; then
    tag_name+="-with-xdebug"
  fi

  docker buildx build \
    --push \
    --platform "${DOCKER_BUILD_PLATFORMS}" \
    --build-arg PHP_VERSION="${php_version}" \
    --build-arg COMPOSER_VERSION="${composer_version}" \
    --build-arg WITH_XDEBUG="${with_xdebug}" \
    --tag "${tag_name}" \
    .
}

build_push "${@}"
